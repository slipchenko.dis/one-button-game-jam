using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{

    [SerializeField] private AudioMixer mainMixer;
   
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider sfxSlider;

    [SerializeField] private float musicVolume;
    [SerializeField] private float sfxVolume;

    void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
        sfxSlider.value = PlayerPrefs.GetFloat("SfxVolume");
    }

    void Update()
    {
        mainMixer.SetFloat("music", musicVolume);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
       
        mainMixer.SetFloat("sfx", sfxVolume);
        PlayerPrefs.SetFloat("SfxVolume", sfxVolume);
    }



    public void SetMusicVolume()
    {
        float volume = musicSlider.value;
        musicVolume = volume;
        mainMixer.SetFloat("music", volume);
    }

    public void SetSFXVolume()
    {
        float volume = sfxSlider.value;
        sfxVolume = volume;
        mainMixer.SetFloat("sfx", volume);
    }


    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit()
    {
       
        Application.Quit();
        Debug.Log("Player has quit the game");
    }

   
}
