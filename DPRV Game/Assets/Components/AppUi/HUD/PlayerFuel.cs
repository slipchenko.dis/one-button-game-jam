using AppLogic.GameControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUi.HUD
{
    public class PlayerFuel : MonoBehaviour
    {
        #region Singleton

        public static PlayerFuel Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion


        [SerializeField] Slider _slider;
        [SerializeField] float _fuel = 10;

        float defaultFuel;

        private void Start()
        {
            defaultFuel = _fuel;
            _slider.maxValue = _fuel;
            _slider.minValue = 0;
            _slider.value = _fuel;

            GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.Instance.GameState);
        }

        private void OnGameStateChanged(GameState gameState)
        {
            switch (gameState)
            {
                case GameState.GamePlay:
                    Fuel = defaultFuel;
                    break;
            }
        }

        public float Fuel 
        { 
            get => _fuel;
            set
            {
                _fuel = value;
                _slider.value = _fuel;
                if(_fuel <= 0)
                {
                   if(GameController.Instance.GameState == GameState.GamePlay) GameController.Instance.GameState = GameState.Lose;
                }
            }
        }
    }
}

