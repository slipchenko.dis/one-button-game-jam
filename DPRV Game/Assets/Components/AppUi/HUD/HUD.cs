using AppLogic.GameControllers;
using AppUI.Dialogs;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.HUD
{
    public class HUD : Page
    {
        [SerializeField] Button _pauseButton;
        [SerializeField] PauseDialog _pauseDialog;

        private void OnEnable()
        {
            _pauseButton.onClick.AddListener(() => _pauseDialog.TogglePause());
        }

        private void OnDisable()
        {
            _pauseButton.onClick.RemoveAllListeners();
        }
    }
}

