using AppLogic.Core;
using AppLogic.GameControllers;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppUI.HUD
{
    public class GamePlayPromt : MonoBehaviour
    {
        [SerializeField] GameObject _promtPanel;
        [SerializeField] TMPro.TMP_Text _promtText;
        [SerializeField] CanvasGroup _canvasGroup;
        [SerializeField] float _showDelay;

        string _promt = "PRESS SPACE\nTO GET ACCELERATED";
        Tween _promtDelayTween;
        Tween _promtTween;

        private void Start()
        {
            HidePromt();

            if (PlayerPrefs.GetInt(PlayerPrefsKeys.GameplayPromt, 0) == 0)
            {
                GameController.Instance.OnGameStateChanged += OnGameStateChanged;
                OnGameStateChanged(GameController.Instance.GameState);
            }
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.GameplayPromt, 1);
                HidePromt();
            }
        }

        private void OnGameStateChanged(GameState gameState)
        {
            switch (gameState)
            {
                case GameState.Menu:
                    OnMenu();
                    break;
                case GameState.GamePlay:
                    OnGamePlay();
                    break;
                case GameState.Finish:
                    OnFinish();
                    break;
            }
        }

        private void OnMenu()
        {
            HidePromt();
        }

        private void OnFinish()
        {
            HidePromt();
        }

        private void HidePromt()
        {
            if (_promtTween != null)
            {
                _promtTween.Kill();
                _promtTween = null;
            }

            if (_promtDelayTween != null)
            {
                _promtDelayTween.Kill();
                _promtDelayTween = null;
            }

            _canvasGroup.alpha = 0f;
            _promtPanel.SetActive(false);
        }

        private void OnGamePlay()
        {
            if (PlayerPrefs.GetInt(PlayerPrefsKeys.GameplayPromt, 0) == 0)
            {
                HidePromt();

                _promtTween = DOVirtual.DelayedCall(_showDelay, () =>
                {
                    _promtPanel.SetActive(true);

                    _promtText.text = _promt;
                    _canvasGroup.alpha = 1f;
                }).SetAutoKill();
            }
        }
    }
}