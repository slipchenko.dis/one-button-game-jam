using AppLogic;
using AppLogic.Camera;
using AppLogic.GameControllers;
using AppUI;
using AppUI.HUD;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace AppUI.RestartPage
{
    public class RestartPage : Page
    {
        [SerializeField] Button _backMenuButton;

        private void OnEnable()
        {
            _backMenuButton.onClick.AddListener(() =>
            {
                OnBackMenuButtonClicked();
            });
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                OnBackMenuButtonClicked();
            }
        }

        private void OnBackMenuButtonClicked()
        {
            if (AppLogic.GameControllers.GameController.Instance.GameState >= AppLogic.GameControllers.GameState.Finish)
            {
                LevelController.Instance.RestartLevel();
                AppLogic.GameControllers.GameController.Instance.GameState = AppLogic.GameControllers.GameState.GamePlay;
            }
        }

        private void OnDisable()
        {
            _backMenuButton.onClick.RemoveAllListeners();
        }
    }
}

