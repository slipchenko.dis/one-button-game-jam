using AppLogic;
using AppLogic.Camera;
using AppUI.HUD;
using DG.Tweening;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Text;
using System;

namespace AppUI.RestartPage
{
    public class WinPage : Page
    {
        [SerializeField] float _counterSpeed;
        [SerializeField] Button _backMenuButton;

        private void OnEnable()
        {
            _backMenuButton.onClick.AddListener(() =>
            {
                OnBackMenuButtonClicked();
            });
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                OnBackMenuButtonClicked();
            }
        }

        private void OnDisable()
        {
            _backMenuButton.onClick.RemoveAllListeners();
        }

        private void OnBackMenuButtonClicked()
        {
            if (AppLogic.GameControllers.GameController.Instance.GameState >= AppLogic.GameControllers.GameState.Finish)
            {
                AppLogic.GameControllers.GameController.Instance.GameState = AppLogic.GameControllers.GameState.Menu;
            }
        }
    }
}

