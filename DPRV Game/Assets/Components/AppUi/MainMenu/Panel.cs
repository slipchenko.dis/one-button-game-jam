using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AppUI.MainMenu
{
    public class Panel : MonoBehaviour
    {
        [SerializeField] PanelType _panelType;

        public PanelType PanelType { get => _panelType; private set => _panelType = value; }

        protected virtual void OnEnable()
        {
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}

