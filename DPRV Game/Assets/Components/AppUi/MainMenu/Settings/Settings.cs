using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.MainMenu.Settings
{
    public class Settings : Panel
    {
        [SerializeField] private Button _close;
        [Header("Volume")]
        [SerializeField] private Slider _musicVolumeSlider;
        [SerializeField] private Slider _soundVolumeSlider;
        [SerializeField] private MainMenu _mainMenu;

        private int _currentLocalizationID = 0;
        public float Sound { get => PlayerPrefs.GetFloat("Sound", .5f); set => PlayerPrefs.SetFloat("Sound", value); }
        public float Music { get => PlayerPrefs.GetFloat("Music", .5f); set => PlayerPrefs.SetFloat("Music", value); }

        private void Start()
        {
            _close.onClick.AddListener(() =>
            {
                _mainMenu.OpenPanel(PanelType.Main);
            });

            _musicVolumeSlider.onValueChanged.AddListener(OnMusicVolumeChanged);
            _soundVolumeSlider.onValueChanged.AddListener(OnSoundVolumeChanged);
            _musicVolumeSlider.value = Music;
            _soundVolumeSlider.value = Sound;
        }

        private void OnMusicVolumeChanged(float value)
        {
            Music = value;

            AppLogic.Audio.AudioManager.Instance.UpdateMusicVolume(value);
        }

        private void OnSoundVolumeChanged(float value)
        {
            Sound = value;
        }
    }
}