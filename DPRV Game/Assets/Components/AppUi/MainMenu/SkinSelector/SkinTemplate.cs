using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinTemplate : MonoBehaviour
{
    [SerializeField] Image _icon;
    [SerializeField] Image _bg;
    [SerializeField] Color _selectedColor;
    [SerializeField] Color _unselectedColor;
    [SerializeField] Button _button;

    public int ID
    {
        get;
        private set;
    }

    public void Initialize(Sprite icon, int id, Action<int> onClick)
    {
        ID = id;
        this._icon.sprite = icon;
        _button.onClick.AddListener(() => onClick.Invoke(id));
    }

    public void SetSelected(bool value)
    {
        _bg.color = value ? _selectedColor : _unselectedColor;
    }
}
