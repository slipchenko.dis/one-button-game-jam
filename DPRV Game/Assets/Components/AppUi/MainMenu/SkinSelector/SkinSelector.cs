using AppLogic.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.MainMenu.Settings
{
    [Serializable]
    public class SkinData
    {
        public int ID; 
        public Sprite Icon; 
    }

    public class SkinSelector : Panel
    {
        [SerializeField] private Button _close;
        [SerializeField] private Transform _contentParent;
        [SerializeField] private SkinTemplate _skinTemplate;
        [SerializeField] private MainMenu _mainMenu;
        [SerializeField] private SkinData[] _skins;

        List<SkinTemplate> _skinTemplates = new List<SkinTemplate>();

        private void Start()
        {
            _close.onClick.AddListener(() =>
            {
                _mainMenu.OpenPanel(PanelType.Main);
            });
        }

        private void Awake()
        {
            foreach (var skin in _skins)
            {
                var newSkin = Instantiate(_skinTemplate, _contentParent);
                newSkin.Initialize(skin.Icon, skin.ID, (arg1) => {
                    SetNewSkin(arg1);
                });

                _skinTemplates.Add(newSkin);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            SetNewSkin(PlayerPrefs.GetInt(PlayerPrefsKeys.PlayerSkin, 0));
        }

        public void SetNewSkin(int id)
        {
            var newSkin = _skinTemplates.Find(x => x.ID == id);
            if (newSkin == null) return;

            foreach (var skin in _skinTemplates)
            {
                skin.SetSelected(false);
            }

            newSkin.SetSelected(true);

            PlayerPrefs.SetInt(PlayerPrefsKeys.PlayerSkin, id);
        }
    }
}