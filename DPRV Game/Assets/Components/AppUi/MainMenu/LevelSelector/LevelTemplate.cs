using AppLogic.GameControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.MainMenu.LevelSelector
{
    public class LevelTemplate : MonoBehaviour
    {
        [SerializeField] TMPro.TMP_Text _level;
        [SerializeField] Button _button;

        public void Initialize(int levelID)
        {
            _level.text = (levelID + 1).ToString();

            _button.onClick.AddListener(() =>{
                LevelController.Instance.SpawnLevel(levelID);
                AppLogic.GameControllers.GameController.Instance.GameState = GameState.GamePlay;
            });
        }
    }
}