using AppLogic.GameControllers;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.MainMenu.LevelSelector
{
    public class LevelSelector : Panel
    {
        [SerializeField] private Button _close;
        [SerializeField] private Transform _contentParent;
        [SerializeField] private LevelTemplate _levelTemplate;
        [SerializeField] private MainMenu _mainMenu;

        private void Start()
        {
            _close.onClick.AddListener(() =>
            {
                _mainMenu.OpenPanel(PanelType.Main);
            });
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            for (int i = _contentParent.childCount - 1; i >= 0; i--)
            {
                Destroy(_contentParent.GetChild(i).gameObject);
            }

            for (int i = 0; i < LevelController.Instance.Levels.Length; i++)
            {
                var newLevel = Instantiate(_levelTemplate, _contentParent);
                newLevel.Initialize(i);
            }
        }
    }
}