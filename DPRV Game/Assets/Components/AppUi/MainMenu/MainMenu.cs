using AppLogic.GameControllers;
using AppUI.Dialogs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.MainMenu
{
    public enum PanelType
    {
        Main,
        Settings,
        LevelSelector,
        SkinSelector
    }

    public class MainMenu : Page
    {
        [SerializeField] List<Panel> _panels;
        [SerializeField] Button _openLevelSelector;
        [SerializeField] Button _openSettings;
        [SerializeField] Button _exitButton;
        [SerializeField] Button _selectSkin;
        [SerializeField] ExitDialog _exitDialog;

        private void OnEnable()
        {
            InitializeEvents();
            OpenPanel(PanelType.Main);
        }

        private void InitializeEvents()
        {
            _exitButton.onClick.AddListener(ExitButtonClicked);
            _openSettings.onClick.AddListener(OpenSettings);
            _openLevelSelector.onClick.AddListener(OpenLevelSelector);
            _selectSkin.onClick.AddListener(OpenSkinSelector);
        }

        private void ExitButtonClicked()
        {
            _exitDialog.ActivateDialog(!_exitDialog.IsActive);
        }

        private void OpenSettings()
        {
            OpenPanel(PanelType.Settings);
        }

        private void OpenLevelSelector()
        {
            OpenPanel(PanelType.LevelSelector);
        }

        private void OpenSkinSelector()
        {
            OpenPanel(PanelType.SkinSelector);
        }

        private void OnDisable()
        {
            _exitButton.onClick.RemoveListener(ExitButtonClicked);
            _openSettings.onClick.RemoveListener(OpenSettings);
            _openLevelSelector.onClick.RemoveListener(OpenLevelSelector);
            _selectSkin.onClick.RemoveListener(OpenSkinSelector);
        }

        public void OpenPanel(PanelType panelType)
        {
            CloseAllPanels();

            var hudPage = GetPageByType(panelType);
            if (hudPage != null)
            {
                hudPage.gameObject.SetActive(true);
            }
        }

        private void CloseAllPanels()
        {
            foreach (var panel in _panels)
            {
                panel.Close();
            }
        }

        private Panel GetPageByType(PanelType pageType)
        {
            return _panels.Where(page => page.PanelType.Equals(pageType)).FirstOrDefault();
        }
    }
}


