using AppLogic.GameControllers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.Dialogs
{
    public class ExitDialog : MonoBehaviour
    {
        public Action<bool> OnSetActive;

        [SerializeField] GameObject _exitDialogGO;
        [SerializeField] GameObject _pauseDialogGO;

        [SerializeField] Button _exitButton;
        [SerializeField] Button _closeDialogButton;

        public bool IsActive
        {
            get
            {
                return _exitDialogGO.activeSelf;
            }
        }

        private void Start()
        {
            _exitButton.onClick.AddListener(() => Application.Quit());
            _closeDialogButton.onClick.AddListener(() => ActivateDialog(false));
        }

        private void Update()
        {
            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape) && GameController.Instance.GameState == GameState.Menu)
            {
                // PauseController.Instance.SetPause(true);
                ActivateDialog(true);
            }
        }

        public void ActivateDialog(bool value)
        {
            if (value == false && _pauseDialogGO.activeSelf)
            {
                _exitDialogGO.SetActive(value);
                OnSetActive?.Invoke(value);
                return;
            }

            Time.timeScale = value ? 0f : 1f;
            _exitDialogGO.SetActive(value);
            OnSetActive?.Invoke(value);
        }
    }
}
