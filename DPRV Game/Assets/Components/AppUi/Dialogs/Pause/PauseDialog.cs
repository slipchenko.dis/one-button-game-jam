using AppLogic.GameControllers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AppUI.Dialogs
{
    public class PauseDialog : MonoBehaviour
    {
        public Action<bool> OnSetActive;

        [SerializeField] GameObject _pauseDialogGO;
        [SerializeField] Button _continueButton;
        [SerializeField] Button _menu;

        bool _paused;

        private void Start()
        {
            _continueButton.onClick.AddListener(() => {
                SetPause(false);
            });

            _menu.onClick.AddListener(() =>
            {
                SetPause(false);
                GameController.Instance.GameState = GameState.Menu;
            });
        }

        public void TogglePause()
        {
            SetPause(!_paused);
        }

        public void SetPause(bool value)
        {
            if (GameController.Instance.GameState == GameState.GamePlay)
            {
                _paused = value;
                _pauseDialogGO.SetActive(value);
                Time.timeScale = value ? 0f : 1f;
                OnSetActive?.Invoke(value);
            }
        }

        private void OnApplicationFocus(bool focus)
        {
            if (focus == false)
                SetPause(true);
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause == true)
                SetPause(true);
        }
    }
}

