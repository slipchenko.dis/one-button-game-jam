using AppLogic.GameControllers;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AppUI
{
    public enum PageType
    {
        MainMenu,
        HUD,
        RestartPage,
        Respawn,
        Win,
        StartPage
    }

    public class UIManager : MonoBehaviour
    {
        #region Singleton
        public static UIManager Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        [SerializeField] private List<Page> _allPages;

        void Start()
        {
            GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.Instance.GameState);
        }

        private void OnGameStateChanged(GameState gameState)
        {
            CloseAllPanels();

            switch (gameState)
            {
                case GameState.Menu:
                    var menuPage = GetPageByType(PageType.MainMenu);
                    if(menuPage != null)
                    {
                        menuPage.gameObject.SetActive(true);
                    }
                    break;
                case GameState.GamePlay:
                    var hudPage = GetPageByType(PageType.HUD);
                    if (hudPage != null)
                    {
                        hudPage.gameObject.SetActive(true);
                    }
                    break;
                case GameState.Win:
                    var winPage = GetPageByType(PageType.Win);
                    if (winPage != null)
                    {
                        winPage.gameObject.SetActive(true);
                    }
                    break;
                case GameState.Lose:
                    var restartPage = GetPageByType(PageType.RestartPage);
                    if (restartPage != null)
                    {
                        restartPage.gameObject.SetActive(true);
                    }
                    break;
                case GameState.Finish:

                    break;
            }
        }

        private Page GetPageByType(PageType pageType)
        {
            return _allPages.Where(page => page.PageType.Equals(pageType)).FirstOrDefault();
        }

        private void CloseAllPanels()
        {
            foreach (var panel in _allPages)
            {
                panel.gameObject.SetActive(false);
            }
        }


    }
}

