using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.GameControllers
{
    public enum GameState
    {
        Menu,
        GamePlay,
        Pause,
        Finish,
        Lose,
        Win,
        LevelSelector
    }

    public class GameController : MonoBehaviour
    {
        public static GameController Instance;

        public event Action<GameState> OnGameStateChanged;
        [SerializeField] private GameState _gameState;

        private void Awake()
        {
            Instance = this;
            GameState = GameState.Menu;
        }

        public GameState GameState
        {
            get
            {
                return _gameState;
            }
            set
            {
                _gameState = value;
                OnGameStateChanged?.Invoke(_gameState);
            }
        }
    }
}

