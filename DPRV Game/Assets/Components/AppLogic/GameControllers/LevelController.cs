using AppLogic.GameControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.GameControllers
{
    public class LevelController : MonoBehaviour
    {
        #region Singleton

        public static LevelController Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public class CurrentLevel
        {
            public int Level;
            public GameObject LevelGO;
        }

        [SerializeField] GameObject[] _levels;
        [SerializeField] Transform _levelParent;

        private CurrentLevel _currentLevel;

        public GameObject[] Levels { get => _levels; }

        private void Start()
        {
            GameController.Instance.OnGameStateChanged += OnGameStateChanged;
            OnGameStateChanged(GameController.Instance.GameState);
        }

        private void OnGameStateChanged(GameState gameState)
        {
            switch (gameState)
            {
                case GameState.Menu:
                    DeleteLevel();
                    break;
            }
        }

        public void SpawnLevel(int levelIndex)
        {
            if (_currentLevel != null)
            {
                Destroy(_currentLevel.LevelGO);
            }

            var newLevel = Instantiate(Levels[levelIndex], _levelParent);

            _currentLevel = new CurrentLevel();
            _currentLevel.LevelGO = newLevel;
            _currentLevel.Level = levelIndex;
        }

        public void DeleteLevel()
        {
            if (_currentLevel != null)
            {
                Destroy(_currentLevel.LevelGO);
            }

            _currentLevel = null;
        }

        public void RestartLevel()
        {
            if (_currentLevel == null) return;

            Destroy(_currentLevel.LevelGO);
            _currentLevel.LevelGO = Instantiate(Levels[_currentLevel.Level], _levelParent);
        }
    }
}
