using AppLogic.GameControllers;
using UnityEngine;
using DG.Tweening;

namespace AppLogic.Portal
{
    public class Finish : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponentInParent<AppLogic.Player.Player>();
            if (player)
            {
                player.PlayerMovement.StopMovement();
                player.transform.DOScale(0.01f, .25f).SetEase(Ease.InBounce).SetAutoKill();
                GameController.Instance.GameState = GameState.Win;
            }
        }
    }
}

