using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Planet
{
    public class Planet : MonoBehaviour
    {
        public float OrbitRadius
        {
            get
            {
                return GetComponent<SphereCollider>().radius;
            }
        } 
    }
}

