using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AppLogic.Audio
{
    public class AudioManager : MonoBehaviour
    {
        #region Singleton
        public static AudioManager Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        public float Sound { get => PlayerPrefs.GetFloat("Sound", .5f); set => PlayerPrefs.SetFloat("Sound", value); }
        public float Music { get => PlayerPrefs.GetFloat("Music", .5f); set => PlayerPrefs.SetFloat("Music", value); }

        [SerializeField] AudioSource _audioSource;
        [SerializeField] AudioSource _audioSourceMusic;
        [SerializeField] AudioClip _music;

        AudioClip _previousSound;
        private float _defaultSoundPitch;
        Tween _soundPitchTween;
        private float _minSoundPitch = .95f;
        private float _soundPitchDecreaseValue = .0085f;

        private void Start()
        {
            _defaultSoundPitch = _audioSource.pitch;
            DOVirtual.DelayedCall(1f, () => PlayMusic(_music)).SetAutoKill();
        }

        public void PlayAudio(AudioClip audioClip)
        {
            if (_audioSource == null) return;

            if (_previousSound == audioClip)
            {
                if (_audioSource.pitch - _soundPitchDecreaseValue >= _minSoundPitch)
                    _audioSource.pitch -= _soundPitchDecreaseValue;

                _soundPitchTween.Kill();
                _soundPitchTween = null;
            }
            else
            {
                _audioSource.pitch = _defaultSoundPitch;
            }

            _audioSource.Stop();
            _audioSource.clip = audioClip;
            _audioSource.volume = Sound;
            _audioSource.Play();

            _previousSound = audioClip;

            if (_soundPitchTween != null)
            {
                _soundPitchTween.Kill();
                _soundPitchTween = null;
            }

            _soundPitchTween = DOVirtual.DelayedCall(1.25f, () =>
            {
                _audioSource.pitch = _defaultSoundPitch;
                _previousSound = null;
            });
        }

        public void PlayMusic(AudioClip audioClip)
        {
            if (_audioSource == null) return;

            _audioSourceMusic.Stop();
            _audioSourceMusic.clip = audioClip;
            _audioSourceMusic.volume = Music / 2.5f;
            _audioSourceMusic.Play();
        }

        public void UpdateMusicVolume(float value)
        {
            Music = value;
            _audioSourceMusic.volume = Music / 2.5f;
        }
    }
}

