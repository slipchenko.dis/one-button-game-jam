using AppLogic.Core;
using Player;
using UnityEngine;

namespace AppLogic.Player
{
    public class Player : MonoBehaviour
    {
        [SerializeField] PlayerMovement _playerMovement;
        [SerializeField] Skin[] _playerSkins;

        public PlayerMovement PlayerMovement { get => _playerMovement; set => _playerMovement = value; }

        private void Start()
        {
            SetSkin();
        }

        private void SetSkin()
        {
            var skinId = PlayerPrefs.GetInt(PlayerPrefsKeys.PlayerSkin, 0);

            foreach (var skin in _playerSkins)
            {
                skin.gameObject.SetActive(false);
                if(skin.SkinID == skinId)
                {
                    skin.gameObject.SetActive(true);
                }
            }
        }
    }
}
