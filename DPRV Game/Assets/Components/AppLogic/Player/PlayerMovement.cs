using AppLogic.GameControllers;
using AppUi.HUD;
using UnityEngine;

namespace AppLogic.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] float speed = 5f;
        [SerializeField] float speedMultiplayer = 2f;
        [SerializeField] float rotationSpeed = 2f;
        [SerializeField] GameObject particlesOnDestroy;
        [SerializeField] AudioClip clipOnDestroy;
        [SerializeField] float orbitOffset = 2.5f;

        private bool isOrbiting = false;
        private Transform target;
        private float orbitRadius = 5f;
        private Vector3 initialDirection;

        public void StopMovement()
        {
            speed = 0f;
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                PlayerFuel.Instance.Fuel -= Time.deltaTime * 30f;
                transform.Translate(Vector3.forward * speed * Time.deltaTime * speedMultiplayer);
            }
            else if (!isOrbiting)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            else
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }

            if (isOrbiting && Vector3.Distance(target.position, transform.position) > orbitRadius + orbitOffset)
            {
                target = null;
                isOrbiting = false;
            }
        }

        private void LateUpdate()
        {
            if (isOrbiting && !Input.GetKey(KeyCode.Space))
            {
                var direction = (target.position) - transform.position;
                var targetRotation = Quaternion.FromToRotation(-transform.up * Mathf.Sign(initialDirection.z), direction);
                targetRotation = Quaternion.Euler(new Vector3(targetRotation.eulerAngles.x, targetRotation.eulerAngles.y, targetRotation.eulerAngles.z));
                var fixedRotation = targetRotation * transform.rotation;

                transform.rotation = Quaternion.Lerp(transform.rotation, fixedRotation, Time.deltaTime * rotationSpeed);
            }

            if (transform.position.x > 0 || transform.position.x < 0)
                transform.position = new Vector3(0, transform.position.y, transform.position.z);
        }

        private void OnTriggerEnter(Collider other)
        {
            var planet = other.GetComponent<AppLogic.Planet.Planet>();
            if (planet)
            {
                target = planet.transform;
                orbitRadius = planet.OrbitRadius;
                initialDirection = transform.forward;
                isOrbiting = true;
            }
        }

        private void OnDestroy()
        {
            if (GameController.Instance.GameState == GameState.GamePlay)
            {
                Instantiate(particlesOnDestroy, transform.position, transform.rotation);
                GameController.Instance.GameState = GameState.Lose;
                Audio.AudioManager.Instance.PlayAudio(clipOnDestroy);
            }
        }
    }
}