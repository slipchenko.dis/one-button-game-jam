using AppLogic.Camera;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Obstacle
{
    public class Obstacle : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponentInParent<Player.Player>();
            if (player)
            {
                CinamechineShake.Instance.ShakeCamera(5, .5f);
                Destroy(player.gameObject);
            }
        }
    }
}

