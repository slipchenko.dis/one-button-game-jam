using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Core
{
    public class MoveBetweenPoints : MonoBehaviour
    {
        [SerializeField] Transform[] _points;
        [SerializeField] float _speed = 1.0f;

        private int _currentPointIndex = 0;

        void Update()
        {
            Vector3 currentPosition = transform.position;
            Vector3 nextPosition = _points[_currentPointIndex].position;

            transform.position = Vector3.MoveTowards(currentPosition, nextPosition, _speed * Time.deltaTime);

            if (Vector3.Distance(currentPosition, nextPosition) < 0.01f)
            {
                _currentPointIndex = (_currentPointIndex + 1) % _points.Length;
            }
        }
    }
}

