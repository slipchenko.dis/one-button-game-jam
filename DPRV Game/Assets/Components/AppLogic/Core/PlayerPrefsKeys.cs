namespace AppLogic.Core
{
    public class PlayerPrefsKeys
    {
        public static readonly string PlayerSkin = "PlayerSkin";
        public static readonly string GameplayPromt = "GameplayPromt";
    }
}