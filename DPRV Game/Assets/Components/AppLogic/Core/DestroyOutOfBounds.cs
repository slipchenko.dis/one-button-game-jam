using UnityEngine;

namespace AppLogic.Core
{
    public class DestroyOutOfBounds : MonoBehaviour
    {
        private void Update()
        {
            Vector3 viewPos = UnityEngine.Camera.main.WorldToViewportPoint(transform.position);
            if (viewPos.x < -0.1f || viewPos.x > 1.1f || viewPos.y < -0.1f || viewPos.y > 1.1f)
            {
                Destroy(gameObject);
            }
        }
    }

}
