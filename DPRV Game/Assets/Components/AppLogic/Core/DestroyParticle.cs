using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticle : MonoBehaviour
{
    [SerializeField] ParticleSystem _particleSystem;
    [SerializeField] bool _destroy;

    void OnEnable()
    {
        StartCoroutine(Autodestroy());
    }

    IEnumerator Autodestroy()
    {
        yield return new WaitForSeconds(_particleSystem.duration);
        if (_destroy)
        {
            Destroy(this.gameObject);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
  
}
