using AppLogic.GameControllers;
using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppLogic.Camera
{
    public class CinamechineShake : MonoBehaviour
    {

        #region Singleton
        public static CinamechineShake Instance;

        private void Awake()
        {
            Instance = this;
        }
        #endregion

        [SerializeField] CinemachineVirtualCamera _cinemachineVirtualCamera;

        private float _shakeTimer;

        public void ShakeCamera(float intensity, float time)
        {
            var cinemachineBasicMultiChannelPerlin =
                _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;

            _shakeTimer = time;
        }

        private void Update()
        {
            if(_shakeTimer > 0)
            {
                _shakeTimer -= Time.deltaTime;

                if(_shakeTimer <= 0f)
                {
                    var cinemachineBasicMultiChannelPerlin =
                        _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                    cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0f;
                }
            }
        }
    }
}
