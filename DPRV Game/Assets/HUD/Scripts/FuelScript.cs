using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class FuelScript : MonoBehaviour
{
    [SerializeField] private Slider fuelSlider;
    [SerializeField] private TMP_Text fuelValueUI;

    public void FuelSlider(float value)
    {
        fuelValueUI.text = value.ToString("0");    
    }
}
